import ObjectJSon.EmotionResponse;
import ObjectJSon.Scores;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.google.gson.stream.JsonToken.END_DOCUMENT;

/**
 * Class that handles the algorithm for the faces recognition and the emotion detection.
 */
public class FaceRecognitionImpl implements FaceRecognition {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    private Sender sender = new SenderImpl();
    private double threshValueLastImage = 0;

    private final double threshold = 0.1;
    private double threshValueImage;

    private final MatOfRect faces = new MatOfRect();

    private Mat image;
    private Mat hsvImage;
    private Mat background;

    private VideoCapture capture;
    private final List<Scores> samePersonScores = new ArrayList<>();
    private final CascadeClassifier classifier = new CascadeClassifier();
    private double threshValueBackground;

    /**
     * Starts the recording, the capturePeriod is a length of time in ms.
     */
    @Override
    public void startRecording() {
        int capturePeriod = 500;
        capture = new VideoCapture();
        capture.open(1);
        classifier.load("res/haarcascade_frontalface_alt.xml");

        System.out.println("Recording Start");
        if (capture.isOpened()) {

                Runnable runnable = () -> {
                    Mat frame = grabFrame();
                    recording(frame);
                };
            try {
                ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();

                timer.scheduleAtFixedRate(runnable, 0, capturePeriod, TimeUnit.MILLISECONDS);
            } catch (Exception e){
                System.err.println(e.getLocalizedMessage());
            }
        }
    }

    /**
     * Grabs a frame from the camera
     * @return the grabbed frame
     */
    private Mat grabFrame() {
        Mat frame = new Mat();
        if (this.capture.isOpened()) {
            try {
                this.capture.read(frame);
                if (!frame.empty()) {
                     this.recording(frame);
                }
            }
            catch (Exception e) {
                System.err.println("Exception during the image elaboration: " + e);
            }
        }
        return frame;
    }

    /**
     * Starts the recording.
     *
     * It grabs the frame every XX seconds.
     * The frame is then converted to an HSV image.
     * The image is split in his layers and its average histogram is calculated and stored.
     * The calculated value is then compared to the background's value.
     * If the value is higher than a fixed threshold (10%) the software starts scanning for a face.
     * If a face is found the image is sent to Windows EmotionApis via a Post Call on a defined server.
     * The Emotion score is then sent to the WebServerApi which stores all the data.
     *
     *
     * @param frame the current frame
     */
    private void recording(Mat frame) {

        if (background == null){
            background = frame;
            Mat hsvBackground = new Mat();
            List<Mat> hsvPlanesBackground = new ArrayList<>();
            hsvBackground.create(background.size(), CvType.CV_8U);
            Imgproc.cvtColor(background, hsvBackground, Imgproc.COLOR_BGR2HSV);
            Core.split(hsvBackground, hsvPlanesBackground);
            threshValueBackground = getHistAverage(hsvBackground, hsvPlanesBackground.get(0));
        }

            image = frame;
            hsvImage = new Mat();
            List<Mat> hsvPlanesImage = new ArrayList<>();
            hsvImage.create(image.size(), CvType.CV_8U);
            Imgproc.cvtColor(image, hsvImage, Imgproc.COLOR_BGR2HSV);
            Core.split(hsvImage, hsvPlanesImage);
            threshValueImage = getHistAverage(hsvImage, hsvPlanesImage.get(0));

            if (threshValueBackground / threshValueImage > 1 + threshold ||
                    threshValueBackground / threshValueImage < 1 - threshold) {
                System.out.println("I may have a face");

                checkFace();

                if (faces.toArray().length > 0) {
                    haveFace();
                } else {
                    threshValueBackground = threshValueImage;
                    System.out.println("The background slightly changed");

                    if (samePersonScores.size() > 0) {
                        sender.sendPersonScores(samePersonScores);
                    }
                }
            } else {
                System.out.println("I have the same background");

                if (samePersonScores.size() > 0) {
                    sender.sendPersonScores(samePersonScores);
                }
            }
            threshValueLastImage = threshValueImage;
            System.out.println("--------------");
            System.gc();
    }

    /**
     * Check if there's a face or not in the current image. It uses OpenCV CascadeClassifier to do its task.
     */
    private void checkFace() {

        int absoluteFaceSize = 0;
        int height = hsvImage.rows();

        if (Math.round(height * 0.2f) > 0) {
            absoluteFaceSize = Math.round(height * 0.2f);
        }

        Mat grayImage = new Mat();
        Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_BGR2GRAY);

        classifier.detectMultiScale(grayImage, faces, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                new Size(absoluteFaceSize, absoluteFaceSize), new Size());
    }

    /**
     * Method that calculates the average Histogram value for a determined image
     *
     * @param hsvImg the image to use in the operation
     * @param hueValues the number of layers to check
     * @return the average value of hsvImg's histogram
     */
    private double getHistAverage(Mat hsvImg, Mat hueValues) {

        double average = 0.0;
        Mat hist_hue = new Mat();
        MatOfInt histSize = new MatOfInt(180);
        List<Mat> hue = new ArrayList<>();
        hue.add(hueValues);

        Imgproc.calcHist(hue, new MatOfInt(0), new Mat(), hist_hue, histSize, new MatOfFloat(0, 179));

        for (int h = 0; h < 180; h++) {
            average += (hist_hue.get(h, 0)[0] * h);
        }

        return average / hsvImg.size().height / hsvImg.size().width;
    }


    /**
     * Method called if i detect a face in my Image. It checks if the person is the same as the last detection
     * or if it's a different one. It then sends the Data to the WebServerApi
     */
    private void haveFace(){
        System.out.println("I have a face");

        String emotionJSon = sender.sendApacheRequest(image);

        JsonReader reader = new JsonReader(new StringReader(emotionJSon));
        JsonToken token;
        EmotionResponse[] response = new EmotionResponse[0];
        try {
            if ((token = reader.peek()) != END_DOCUMENT && token != null ) {
                switch (token){
                    case BEGIN_ARRAY:
                         response = new Gson().fromJson(emotionJSon, EmotionResponse[].class);
                        break;
                    case BEGIN_OBJECT:
                        if (samePersonScores.size() > 0)
                        sender.sendPersonScores(samePersonScores);
                        System.err.println("Quota Exceeded, Can't get the emotion");
                        return;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (threshValueLastImage != 0) {

            if (threshValueLastImage / threshValueImage < 1 + threshold &&
                    threshValueLastImage / threshValueImage > 1 - threshold) {

                System.out.println("Same person as the last one");

                if (response.length > 0) {
                    samePersonScores.add(response[0].getScores());
                }
            } else {
                sender.sendPersonScores(samePersonScores);
                if (response.length > 0){
                    samePersonScores.add(response[0].getScores());
                }

                System.out.println("Different Person");
            }
        } else {
            System.out.println("It's the first frame");
        }
    }
}