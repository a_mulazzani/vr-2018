import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Utility class
 */
public class Utility {
    private static final Utility baseUrl = new Utility();


    private final String path = System.getProperty("user.home") + File.separator + "SCTM" + File.separator + "config.json";

    public static Utility getInstance() {
        return baseUrl;
    }

    private String url;
    private int idDevice;

    private Utility() {

    }


    public String getPath() {
        return path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(int idDevice) {
        this.idDevice = idDevice;
    }

    /**
     * Saves a configuration JSON on file
     */
    public void saveOnDisk(){
        try {

            File file = new File(path);
            file.getParentFile().mkdir();
            file.createNewFile();

            FileWriter writer = new FileWriter(path);
            Gson gson = new Gson();
            String toWrite = gson.toJson(Utility.getInstance());
            System.out.println(toWrite);
            writer.write(gson.toJson(Utility.getInstance()));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
