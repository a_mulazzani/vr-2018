import ObjectJSon.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.opencv.core.Mat;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *Class that handles the messages between the device and the server.
 */
public class SenderImpl implements Sender {

    @Override
    public void sendPersonScores(List<Scores> samePersonScores) {

        Scores averageScores = calculateAverageEmotion(samePersonScores);
        samePersonScores.clear();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        int idDetection = checkDate(date);
        if(idDetection == -1) {
            idDetection = sendDetection(date);
        }
        if (averageScores.getHappiness() > 0) {
            sendScores(idDetection, averageScores);
        }
    }


    @Override
    public String sendApacheRequest(Mat image) {

        System.out.println("I'm sending the Request to the EmotionApi");

        try {
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
            String result = "";

            URIBuilder builder = new URIBuilder("https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize");
            URI uri = builder.build();

            HttpPost request = new HttpPost(uri);
            request.setHeader("Content-Type", "application/octet-stream");
            request.setHeader("Ocp-Apim-Subscription-Key", "e1504c95cabd49b69bfbe63199b98e6e");

            BufferedImage bufferedImage =  mat2BufferedImage(image);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DateFormat dateFormat = new SimpleDateFormat("HH-mm-ss");
            Date tempDate = new Date();
            File tempFile = new File("res/"+ dateFormat.format(tempDate)+".jpg");
            ImageIO.write(bufferedImage, "jpg", tempFile);
            baos.flush();
            baos.close();

            FileEntity reqEntity = new FileEntity(tempFile);
            request.setEntity(reqEntity);

            CloseableHttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();
            request.setEntity(reqEntity);

            if (entity != null) {
                result = EntityUtils.toString(entity);
                System.out.println(result);
                Files.deleteIfExists(tempFile.toPath());
            }

            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    /**
     * Sends the provided string to the WebServerApi
     * @param date the date to send
     * @return a code which defines the status of the operation
     */
    private int sendDetection(String date) {

        AddDetectionMessage detectionMessage = new AddDetectionMessage();
        detectionMessage.setDataRilevazione(date);

        Gson json = new Gson();
        String string = json.toJson(detectionMessage);

        HttpPost request = new HttpPost(Utility.getInstance().getUrl()+"/rilevazioni.insert/");

        String result = sendMessage(request, string);


        GetAddDetectionMessage codeResponse = json.fromJson(result, GetAddDetectionMessage.class);
        if (codeResponse.getCode() == 1) {
            System.out.println("Detection successfully added!");
            return codeResponse.getMessage().getId();
        } else {
            System.out.println("Error while adding the Detection!");
        }

        return -1;
    }

    /**
     * Sends an HttpPost request and returns its response.
     * @param request The request to fulfill
     * @param string The content of the String body of the Request
     * @return the response from the Server
     */
    private String sendMessage(HttpPost request, String string) {

        String result = "";

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {
            StringEntity params = new StringEntity(string);
            request.addHeader("content-type", "application/json");
            request.setEntity(params);

            CloseableHttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                result = EntityUtils.toString(entity);
            }

            httpClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Sends the scores to the Server
     * @param idDetection the Detection id
     * @param averageScores the scores to send
     */
    private void sendScores(int idDetection, Scores averageScores) {

        String result;

        AddEmotionMessage emotionMessage = new AddEmotionMessage();
        Gson emotionJson = new Gson();
        String stringa = emotionJson.toJson(averageScores);
        System.out.println("EmotionJson " + stringa);
        emotionMessage.setEmozioni(stringa);
        emotionMessage.setIdRilevazione(idDetection);
        emotionMessage.setIdDevice(Utility.getInstance().getIdDevice());

        Gson json = new GsonBuilder().setLenient().create();
        String string = json.toJson(emotionMessage);

        HttpPost request = new HttpPost(Utility.getInstance().getUrl()+"/emozioni.insert/");

        result = sendMessage(request,string);
        CodeMessage response = json.fromJson(result, CodeMessage.class);

        if (response.getCode() == 1) {
            System.out.println("Emotion successfully added!");
        } else {
            System.out.println("Error while adding the Emotion");
        }
    }

    /**
     * Checks if a provided Date is already registered in the system
     * @param date the date to check
     * @return the id of the provided date
     */
    private int checkDate(String date) {

        Gson json = new Gson();
        CodeMessage codeResponse;

        try {
            URL url = new URL(Utility.getInstance().getUrl()+"/rilevazioni.checkData/"+date);
            URLConnection urlConnection = url.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            urlConnection.getInputStream()));

            String inputLine = in.readLine();
            in.close();

            codeResponse = json.fromJson(inputLine, CodeMessage.class);
            if (codeResponse.getCode() == 1) {
                System.out.println("The date is already present!");
                GetDetectionIDMessage detectionIDMessage = json.fromJson(inputLine, GetDetectionIDMessage.class);
                return detectionIDMessage.getMessage().getIdRilevazioniGiornaliere();
            } else {
                System.out.println("The date is not already present!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Converts a OpenCV's Mat object to a BufferedImage
     * @param m the mat to convert
     * @return the BufferedImage
     */
    private BufferedImage mat2BufferedImage(Mat m){
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( m.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels()*m.cols()*m.rows();
        byte [] b = new byte[bufferSize];
        m.get(0,0,b);
        BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    /**
     * Calculates the average value for the set of Scores
     * @return the Average scores
     */
    private Scores calculateAverageEmotion(List<Scores> samePersonScores) {
        Scores averageScores = new Scores();

        for (Scores s: samePersonScores) {
            averageScores.addScore(s);
        }
        averageScores.averageScores(samePersonScores.size());

        return averageScores;
    }
}

