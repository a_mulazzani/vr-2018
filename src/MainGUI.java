import com.google.gson.Gson;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Gui for the application. It has 2 buttons to init the raspberry and to start the camera
 */
public class MainGUI extends JFrame {

    public MainGUI() {

        System.out.println(System.getProperty("java.library.path"));
        setTitle("Emotion2Knowledge");

        boolean init = checkConfig();

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        JButton configButton = new JButton("Init Device");
        JButton startButton = new JButton("Start Camera");
        configButton.setMaximumSize(new Dimension((int) width / 4, (int) height / 17));
        startButton.setMaximumSize(new Dimension((int) width / 4, (int) height / 17));

        configButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        startButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        panel.add(Box.createRigidArea(new Dimension(0, (int) (height / 13))));
        panel.add(configButton);
        panel.add(Box.createRigidArea(new Dimension(0, (int) (height / 7))));
        panel.add(startButton);

        panel.setPreferredSize(new Dimension((int) width / 2, (int) height / 2));
        this.add(panel);

        configButton.addActionListener(e -> EventQueue.invokeLater(() -> {
            ConfigGUI configGui = new ConfigGUI();
            configGui.setVisible(true);
            dispose();
        }));

        if (init) {
            getRootPane().setDefaultButton(startButton);
        } else {
            getRootPane().setDefaultButton(configButton);
        }
        startButton.setEnabled(init);
        configButton.setEnabled(!init);

        startButton.addActionListener(e -> {
            FaceRecognition faceRecognition = new FaceRecognitionImpl();
            faceRecognition.startRecording();
        });

        setSize((int) width / 2, (int) height / 2);
        setMinimumSize(new Dimension((int) width / 4, (int) height / 2));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Check if there's a configuration file already in the system
     * @return TRUE if there's a configuration, FALSE otherwise
     */
    private boolean checkConfig() {

        File file = new File(Utility.getInstance().getPath());

        if (file.exists()) {

            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                StringBuilder builder = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    builder.append(line);
                    builder.append(System.lineSeparator());
                    line = br.readLine();
                }

                String json = builder.toString();
                br.close();
                Gson gson = new Gson();
                Utility util = gson.fromJson(json, Utility.class);
                Utility.getInstance().setIdDevice(util.getIdDevice());
                Utility.getInstance().setUrl(util.getUrl());

                return true;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            MainGUI gui = new MainGUI();
            gui.setVisible(true);
        });
    }
}
