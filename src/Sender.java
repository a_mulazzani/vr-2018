import ObjectJSon.Scores;
import org.opencv.core.Mat;

import java.util.List;

public interface Sender {
    /**
     *
     * Checks if the date is present on the server and sends the Person scores
     * @param samePersonScores scores of the same person
     */
    void sendPersonScores(List<Scores> samePersonScores);

    /**
     * Sends the image data to Microsoft EmotionApis
     * @param image is the image to send
     * @return the JSON which contains the results
     */
    String sendApacheRequest(Mat image);
}
