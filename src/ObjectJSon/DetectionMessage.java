package ObjectJSon;

/**
 *
 * JSON received which contains the Detection's ID with the current date if present
 */
public class DetectionMessage {
    private int idRilevazioniGiornaliere;

    public int getIdRilevazioniGiornaliere() {
        return idRilevazioniGiornaliere;
    }

}
