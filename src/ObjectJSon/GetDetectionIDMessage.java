package ObjectJSon;

/**
 *
 * JSON received which contains the current date, if present.
 */
public class GetDetectionIDMessage {
    private int code;
    private DetectionMessage message;


    public int getCode() {
        return code;
    }

    public DetectionMessage getMessage() {
        return message;
    }
}
