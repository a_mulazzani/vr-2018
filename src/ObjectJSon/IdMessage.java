package ObjectJSon;

/**
 *
 * JSON received when an Add message is sent, which contains the last-added id
 */
public class IdMessage {
    private int id;

    public int getId() {
        return id;
    }
}
