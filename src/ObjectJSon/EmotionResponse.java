package ObjectJSon;

/**
 * JSON received. Scores are the emotion scores.
 */
public class EmotionResponse {

    private Scores scores;

    public Scores getScores() {
        return scores;
    }
}
