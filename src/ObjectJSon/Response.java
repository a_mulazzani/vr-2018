package ObjectJSon;

/**
 *
 * JSON received which contains the message and the id
 */
public class Response {
    private int code;
    private MessageResponse message;


    public int getCode() {
        return code;
    }

    public MessageResponse getMessage() {
        return message;
    }
}
