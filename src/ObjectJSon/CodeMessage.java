package ObjectJSon;

/**
 *
 *JSON received after an Add Message. The Code value can be 0 or 1
 */
public class CodeMessage {
    private int code;

    public int getCode() {
        return code;
    }
}
