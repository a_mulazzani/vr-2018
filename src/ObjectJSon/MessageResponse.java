package ObjectJSon;

/**
 *
 *  JSON received which contains the last added id.
 */
public class MessageResponse {

    public int getId() {
        return id;
    }

    private int id;
}
