package ObjectJSon;

/**
 * Object that defines a set of Emotion's scores.
 */

public class Scores {

    private double anger;
    private double contempt;
    private double disgust;
    private double fear;
    private double happiness;
    private double neutral;
    private double sadness;
    private double surprise;


    private double getAnger() {
        return anger;
    }

    private double getContempt() {
        return contempt;
    }

    private double getDisgust() {
        return disgust;
    }

    private double getFear() {
        return fear;
    }

    public double getHappiness() {
        return happiness;
    }

    private double getNeutral() {
        return neutral;
    }

    private double getSadness() {
        return sadness;
    }

    private double getSurprise() {
        return surprise;
    }

    public void addScore(Scores otherScore){
        this.anger += otherScore.getAnger();
        this.contempt += otherScore.getContempt();
        this.disgust+= otherScore.getDisgust();
        this.fear+= otherScore.getFear();
        this.happiness += otherScore.getHappiness();
        this.sadness += otherScore.getSadness();
        this.surprise += otherScore.getSurprise();
        this.neutral += otherScore.getNeutral();
    }

    public void averageScores(int lenght){
        this.anger /= lenght;
        this.contempt /= lenght;
        this.disgust /= lenght;
        this.fear/= lenght;
        this.happiness /= lenght;
        this.sadness /= lenght;
        this.surprise /= lenght;
        this.neutral /= lenght;
    }


}
