package ObjectJSon;

/**
 *
 *
 * JSON received after an Add message to get its Id
 */
public class GetAddDetectionMessage {
    private int code;
    private IdMessage message;

    public int getCode() {
        return code;
    }

    public IdMessage getMessage() {
        return message;
    }
}
