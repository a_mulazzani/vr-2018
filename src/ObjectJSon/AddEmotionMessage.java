package ObjectJSon;

/**
 *
 *
 * JJSON Sent to add an Emotion
 */
public class AddEmotionMessage {
    private String emozioni;
    private int idDispositivo;
    private int idRilevazione;

    public void setEmozioni(String emozioni) {
        this.emozioni = emozioni;
    }

    public void setIdDevice(int idDevice) {
        this.idDispositivo = idDevice;
    }

    public void setIdRilevazione(int idRilevazione) {
        this.idRilevazione = idRilevazione;
    }
}
