import ObjectJSon.Device;
import ObjectJSon.Response;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;

/**
 *
 *
 * The ConfigGUI class is used to initialize the Device. It takes a Server URL and a name, which are
 * sent to the Server to load the correct Device ID. The Device ID and the Server are stored in the user home.
 *
 */
public class ConfigGUI extends JFrame{


    public ConfigGUI() {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));

        JLabel nameLabel = new JLabel("Nome Raspberry:");
        JLabel urlLabel = new JLabel("URL Server:");
        final JTextField nameText = new JTextField("aa");
        final JTextField urlText = new JTextField("http://progettosctm.altervista.org/");

        JButton sendButton = new JButton("Invia Dati");
        sendButton.setMaximumSize(new Dimension((int)width/4, (int)height/17));

        sendButton.addActionListener(e -> {
            if (!nameText.getText().isEmpty() && !urlText.getText().isEmpty()) {
                String url = urlText.getText();
                if (isURL(url)) {
                    Utility.getInstance().setUrl(url);
                    try {
                        sendRequest(url, nameText.getText());
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "URL non corretto!");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Inserire i dati necessari!");
            }
        });
        getRootPane().setDefaultButton(sendButton);

        nameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        nameText.setAlignmentX(Component.CENTER_ALIGNMENT);
        urlLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        urlText.setAlignmentX(Component.CENTER_ALIGNMENT);
        sendButton.setAlignmentX(Component.CENTER_ALIGNMENT);

        nameText.setMaximumSize(new Dimension((int)width/4, (int)height/20));
        urlText.setMaximumSize(new Dimension((int)width/4, (int)height/20));

        panel.add(Box.createRigidArea(new Dimension(0,(int)(height/13))));
        panel.add(nameLabel);
        panel.add(nameText);
        panel.add(Box.createRigidArea(new Dimension(0,(int)(height/13))));
        panel.add(urlLabel);
        panel.add(urlText);
        panel.add(Box.createRigidArea(new Dimension(0,(int)(height/13))));
        panel.add(sendButton);

        this.add(panel);

        setSize((int) width / 2, (int) height / 2);
        setMinimumSize(new Dimension((int) width / 4, (int) height / 2));
        setLocationRelativeTo(null);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                dispose();
                EventQueue.invokeLater(() -> {
                    MainGUI gui = new MainGUI();
                    gui.setVisible(true);
                });
            }
        });

    }

    private static boolean isURL(String url) {
        try {
            new URL(url);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void sendRequest(String url, String name) throws Exception {

        String result;

        Gson json = new Gson();
        String string = json.toJson(new Device(name));

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(url+"/dispositivi.insert/");
        StringEntity params = new StringEntity(string);
        request.addHeader("content-type", "application/json");
        request.setEntity(params);

        CloseableHttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();

        if (entity != null) {
            result = EntityUtils.toString(entity);

            Response deviceResponse = json.fromJson(result, Response.class);
            if (deviceResponse.getCode() == 1) {
                Utility.getInstance().setIdDevice(deviceResponse.getMessage().getId());
                Utility.getInstance().saveOnDisk();
                JOptionPane.showMessageDialog(null, "Dispositivo aggiunto correttamente!");
                this.dispose();

                EventQueue.invokeLater(() -> {
                    MainGUI gui = new MainGUI();
                    gui.setVisible(true);
                });
            } else {
                JOptionPane.showMessageDialog(null, "Si è verificato un errore!");
            }
        }

        httpClient.close();

    }
}
